Changelog
=========

v0.1.3 (2021-03-12)
-------------------

* (#18) Fix crashes with Z3 2020
* Enable the flag 'includerestricted' for all queries on WS Availability to fetch stats of restricted networks
* Use the extended network code for all queries on WS Statistics with temporary networks

v0.1.2 (2021-02-24)
-------------------

* (#15) Enhancements on the HTML report layout
* (#16) Workaround for the GeoPandas/Naturalearthdata bug on country ISO A3 codes

v0.1.1 (2021-02-18)
-------------------

* Fix packaging: Some files were missing

v0.1.0 (2021-02-17)
-------------------

* First packaged release

v0.0.x (Unreleased)
-------------------

* Internal developments