# encoding: utf8

class NoDataError(Exception):
    pass


class ApiError(Exception):
    pass
