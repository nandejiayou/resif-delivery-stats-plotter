# encoding: utf8
from .. import ServiceAbstract


class ServiceClientAbstract(ServiceAbstract):
    """
    Base class of webservices' clients
    """
    pass
